-module(recursion).
-export([cut/1, fib/1]).

fib(0) ->
    0;
fib(1) ->
    1;
fib(N) when N > 1 ->
    fib(N - 2) + fib(N - 1).

cut(0) ->
    1;
cut(N) when N > 0 ->
    cut(N - 1) + N.
