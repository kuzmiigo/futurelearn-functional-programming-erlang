-module(hofpractice).
-export([doubleAll/1, evens/1, product/1, zip/2, zip2/2, zip_with/3,
         zip_with2/3]).

doubleAll(List) ->
    lists:map(fun (X) -> X * 2 end, List).

evens(List) ->
    lists:filter(fun (X) -> X rem 2 == 0 end, List).

product(List) ->
    lists:foldr(fun (X, Prod) -> X * Prod end, 1, List).


zip(List1, List2) ->
    lists:reverse(zip(List1, List2, [])).

zip([], _Ys, Res) -> Res;
zip(_Xs, [], Res) -> Res;
zip([X|Xs], [Y|Ys], Res) -> zip(Xs, Ys, [{X, Y} | Res]).


zip_with(F, List1, List2) ->
    lists:reverse(zip_with(F, List1, List2, [])).

zip_with(_F, [], _Ys, Res) -> Res;
zip_with(_F, _Xs, [], Res) -> Res;
zip_with(F, [X|Xs], [Y|Ys], Res) -> zip_with(F, Xs, Ys, [F(X, Y) | Res]).

zip_with2(F, List1, List2) ->
    lists:map(fun ({X, Y}) -> F(X, Y) end, zip(List1, List2)).

zip2(List1, List2) ->
    zip_with(fun (X, Y) -> {X, Y} end, List1, List2).
