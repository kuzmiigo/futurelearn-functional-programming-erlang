# Functional programming in Erlang. Week 2

Step 2.20: Programming challenge: indexing a file.

Author: **bojan sovilj**.

## Submission

File: `bojan.erl`.

## Review

**Document the solution: make sure that each function has a clearly identified
purpose, and that, when appropriate, test data is provided to illustrate its
purpose.**

Every function has a short comment, although it is not clear how `word_index`
function works and what is its purpose.

**If submitting a partial solution, make clear what has and hasn’t been solved,
to provide a clear picture of the scope of the solution.**

The solution is incomplete and consists of a few building blocks only. Some of
them seem incorrect, like `my_words`, which returns a list of lists of strings
instead of a list of strings. To fix it, you can replace

    [string:tokens(Line, " ,.!:'-") | my_words(Text)]

with

    string:tokens(Line, " ,.!:'-") ++ my_words(Text)

There are no comments about the scope of the solution and missing pieces.

**If taking the problem further, explain what has been done; otherwise, comment
on how the remaining parts of the problem might be approached.**

You can try to look at solution discussion in the comments at the assignment
page.
