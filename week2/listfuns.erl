-module(listfuns).
-export([maximum/1, maximumDR/1, product/1, productDR/1]).

product(Xs) ->
    product(Xs, 1).

product([], P) ->
    P;
product([X|Xs], P) ->
    product(Xs, X * P).

productDR([]) ->
    1;
productDR([X|Xs]) ->
    X * product(Xs).


maximum([X|Xs]) ->
    maximum(Xs, X).

maximum([], M) ->
    M;
maximum([X|Xs], M) ->
    maximum(Xs, max(M, X)).

maximumDR([X]) ->
    X;
maximumDR([X|Xs]) ->
    max(X, maximumDR(Xs)).
