# Functional programming in Erlang. Week 2

Step 2.20: Programming challenge: indexing a file.

## Submission

Link to the solution file: https://gitlab.com/snippets/1650121

## Review

Submitted by **Germán Suárez Rodríguez**.

**Document the solution: make sure that each function has a clearly identified
purpose, and that, when appropriate, test data is provided to illustrate its
purpose.**

File is well commented. Maybe I missed a general comment about the solution
chosen.

**If submitting a partial solution, make clear what has and hasn’t been solved,
to provide a clear picture of the scope of the solution.**

Full correct solution. Only missing the hard part of normalizing word endings,
but it even includes a list of excluding words, which is quite nice.

**If taking the problem further, explain what has been done; otherwise, comment
on how the remaining parts of the problem might be approached.**

The solution is brilliant, although it's using higher-order functions from week
3: foldl, filter or even using maps to store the words. Not like I'm against
it, but I just wanted to mention it. Nice work!
