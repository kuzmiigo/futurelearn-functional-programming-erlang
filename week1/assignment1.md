# Functional programming in Erlang. Week 1

Step 1.24: Pulling it all together.

## Submission

Links to the solution files:
* Shapes: https://gitlab.com/snippets/1638285
* Bits: https://gitlab.com/snippets/1638286

## Review

Submitted by **Ed S**.

**Ensure that the solution is correct; perhaps include some tests to show how a
function works in a number of representative cases.**

The solutions is clear, all functions are small and easy to understand.

**Ensure that the solution is readable; for example by including comments that
explain how the functions work, and indicate any particular aspects that need
explanation.**

All code contains all needed comments to grasp the ideas behind the solutions.

**Comment on any potential alternative approaches that may have been
considered, and why the one chosen is the most appropriate.**

I liked two versions of solution for calculating number of bits - directive
recursion and tail recursion. As for me in most difficult tasks it's simpler to
start with direct recursion. That helps me to write a solution and probe
several test cases and than to optimize it to tail recursion.
