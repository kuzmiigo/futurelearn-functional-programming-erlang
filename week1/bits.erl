-module(bits).
-export([bits/1, bitsDirect/1]).

%% Finds the sum of the bits in the binary representation of the given number.
%% Tail-recursive version.
%% Examples:
%%     > bits:bits(127).
%%     7
%%     > bits:bits(128).
%%     1
bits(X) ->
    bits(X, 0).

%% Helper function to find the sum of the bits of the given number.
bits(0, Sum) ->
    % If number is 0, there are no more 1's, return the sum.
    Sum;
bits(X, Sum) when X > 0 ->
    % Otherwise, repeat tail recursively, with the number shifted right by one
    % bit (i.e. divided by 2) and the sum is incremented by the value of the
    % least significant bit (binary AND of X and 1).
    bits(X div 2, Sum + X band 1).


%% Finds the sum of the bits in the binary representation of the given number.
%% Direct-recursive version.
bitsDirect(0) ->
    % If number is 0, there are no more 1's, return 0.
    0;
bitsDirect(X) ->
    % Otherwise, repeat recursively, with the number shifted right by one bit
    % (i.e. divided by 2) and add the value of the least significant bit
    % (binary AND of X and 1).
    bitsDirect(X div 2) + X band 1.
