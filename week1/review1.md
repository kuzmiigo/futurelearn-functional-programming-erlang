# Functional programming in Erlang. Week 1

Step 1.24: Pulling it all together.

Author: **Peter Harrad**.

## Submission

File: `phweek1.erl`.

> Code placed into...
>
> https://gist.github.com/peterharrad/42f947bbb87e3268d4e6ad833a8a8770

## Review

**Ensure that the solution is correct; perhaps include some tests to show how a
function works in a number of representative cases.**

Tested on Erlang 19.2.

 1. There is no module definition, so it does not compile.

 2. Unused variables causing compilation warnings:
    ```
    phweek1.erl:6: no module definition
    phweek1.erl:28: Warning: variable 'X' is unused
    phweek1.erl:28: Warning: variable 'Y' is unused
    phweek1.erl:30: Warning: variable 'X' is unused
    phweek1.erl:30: Warning: variable 'Y' is unused
    phweek1.erl:38: Warning: variable 'X' is unused
    phweek1.erl:38: Warning: variable 'Y' is unused
    phweek1.erl:40: Warning: variable 'X' is unused
    phweek1.erl:40: Warning: variable 'Y' is unused
    ```

 3. Some of the provided tests fail:
    ```
    73> phweek1:test_enclose_circle().
    ** exception error: an error occurred when evaluating an arithmetic expression
         in function  phweek1:enclose/1 (phweek1.erl, line 48)
         in call from phweek1:test_enclose_circle/1 (phweek1.erl, line 148)
    75> phweek1:test_enclose_triangle().
    The area of a triangle with with vertices at (15,15),(23,30),(50,25) is enclosed by ** exception error: bad argument
         in function  io:format/3
            called as io:format(<0.50.0>,
                                "  a rectangle with width ~B and height ~B and centre at ~B,~B~n",
                                [35,15,32.5,22.5])
    ```

 4. bitsDR and bitsTR do not work with 0.

 5. Incorrect centre coordinates for enclosing rectangle for circles. As the
    comments state that rectangle definition is taken from the course, X and Y
    in {rectangle,{X,Y},H,W} denote the centre of the rectangle. The centres of
    a circle and its enclosing rectangle are the same:
    ```
    enclose({circle,{X,Y},R}) ->
        {rectangle,{X,Y},2*R,2*R};
    ```

**Ensure that the solution is readable; for example by including comments that
explain how the functions work, and indicate any particular aspects that need
explanation.**

The code is readable and well documented.

**Comment on any potential alternative approaches that may have been
considered, and why the one chosen is the most appropriate.**

Comments show alternative approaches and reasoning behind the selected ones.
