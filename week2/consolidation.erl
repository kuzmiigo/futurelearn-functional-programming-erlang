-module(consolidation).
-export([concat/1, concatF/1, concatTR/1, join/2, member/2, reverse/1]).
-export([isort/1, msort/1, qsort/1, qsort0/1]).
-export([perms/1, perms0/1, perms2/1, perms3/1]).

%% Joins two lists together.
%%     join("hel", "lo") = "hello"
join(Xs, Ys) ->
    join(Xs, [], Ys).

join([], [], Ys) ->
    Ys;
join([], [R | Rs], Ys) ->
    join([], Rs, [R | Ys]);
join([X | Xs], Rs, Ys) ->
    join(Xs, [X | Rs], Ys).

%% Joins a list of lists into a single list. Direct recursion.
%%     concat(["goo","d","","by","e"]) = "goodbye"
concat([]) ->
    [];
concat([X | Xs]) ->
    join(X, concat(Xs)).

%% Joins a list of lists into a single list. Fold right.
concatF(Xs) ->
    lists:foldr(fun join/2, [], Xs).

%% Joins a list of lists into a single list. Tail recursion.
concatTR(Xs) ->
    concatTR(reverse(Xs), []).

concatTR([], Res) ->
    Res;
concatTR([X | Xs], Res) ->
    concatTR(Xs, join(X, Res)).

reverse(Xs) ->
    reverse(Xs, []).

reverse([], Res) ->
    Res;
reverse([X | Xs], Res) ->
    reverse(Xs, [X | Res]).

%% Tests whether its first argument is a member of its second argument, which
%% is a list.
%%     member(2,[2,0,0,1]) = true
%%     member(20,[2,0,0,1]) = false
member(_X, []) ->
    false;
member(X, [X | _Ys]) ->
    true;
member(X, [_Y | Ys]) ->
    member(X, Ys).

%% Merge sort.
msort([]) ->
    [];
msort([X]) ->
    [X];
msort(List) ->
    {List1, List2} = lists:split(length(List) div 2, List),
    merge(msort(List1), msort(List2)).

merge(List1, List2) ->
    merge(List1, List2, []).

merge([], List2, Res) ->
    lists:reverse(Res, List2);
merge(List1, [], Res) ->
    merge([], List1, Res);
merge([X | Xs] = List1, [Y | Ys] = List2, Res) ->
    case X < Y of
        true ->
            merge(Xs, List2, [X | Res]);
        false ->
            merge(List1, Ys, [Y | Res])
    end.

%% Quicksort.
qsort([]) ->
    [];
qsort([X]) ->
    [X];
qsort([X | Xs]) ->
    {List1, List2} = lists:partition(fun(A) -> A < X end, Xs),
    lists:append([qsort(List1), [X], qsort(List2)]).

%% Quicksort. Version 0.
%% http://erlang.org/doc/programming_examples/list_comprehensions.html
qsort0([Pivot|T]) ->
    qsort0([ X || X <- T, X < Pivot]) ++
    [Pivot] ++
    qsort0([ X || X <- T, X >= Pivot]);
qsort0([]) -> [].

%% Insertion sort.
isort([]) ->
    [];
isort([X]) ->
    [X];
isort([X | Xs]) ->
    insert(X, [], isort(Xs)).

insert(X, Left, []) ->
    lists:reverse(Left, [X]);
insert(X, Left, [Y | Ys] = Right) ->
    case X < Y of
        true ->
            lists:reverse(Left, [X | Right]);
        false ->
            insert(X, [Y | Left], Ys)
    end.


%%
%% Permutations. Version 1.
%%
perms([]) ->
    [[]];
perms([X | Xs]) ->
    procPerm(X, perms(Xs), []).

%% Adds the given item (first parameter) to every permutation in the list.
%% All generated permutations are added to the given accumulator.
procPerm(_, [], Acc) ->
    Acc;
procPerm(X, [P | Ps], Acc) ->
    procPerm(X, Ps, genPerm(X, [], P, Acc)).

%% Inserts the given item (first parameter) into every possible position of
%% the given list, divided into left and right parts. All generated
%% permutations are added to the given accumulator.
genPerm(X, Left, [], Acc) ->
    [lists:reverse(Left, [X]) | Acc];
genPerm(X, Left, [Y | Ys] = Right, Acc) ->
    genPerm(X, [Y | Left], Ys, [lists:reverse(Left, [X | Right]) | Acc]).


%%
%% Permutations. Version 2.
%%
perms2([]) ->
    [[]];
perms2([X | Xs]) ->
    procPerm2(X, perms2(Xs), []).

procPerm2(_, [], Acc) ->
    Acc;
procPerm2(X, [P | Ps], Acc) ->
    procPerm2(X, Ps, genPerm2(X, [], P, [[X | P] | Acc])).

genPerm2(_X, _Left, [], Acc) ->
    Acc;
genPerm2(X, Left, [Y | Ys], Acc) ->
    NewLeft = [Y | Left],
    genPerm2(X, NewLeft, Ys, [lists:reverse(NewLeft, [X | Ys]) | Acc]).


%%
%% Permutations. Version 3 (version 2 + flatmap).
%%
perms3([]) ->
    [[]];
perms3([X | Xs]) ->
    lists:flatmap(fun(P) -> genPerm3(X, [], P, [[X | P]]) end, perms3(Xs)).

genPerm3(_X, _Left, [], Acc) ->
    Acc;
genPerm3(X, Left, [Y | Ys], Acc) ->
    NewLeft = [Y | Left],
    genPerm3(X, NewLeft, Ys, [lists:reverse(NewLeft, [X | Ys]) | Acc]).


%%
%% Permutations. Version 0.
%% http://erlang.org/doc/programming_examples/list_comprehensions.html
%%
perms0([]) -> [[]];
perms0(L)  -> [[H|T] || H <- L, T <- perms0(L--[H])].
