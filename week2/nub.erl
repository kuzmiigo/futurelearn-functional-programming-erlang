-module(nub).
-export([bun/1, nub/1]).

%% Keeps first occurrences.
%%     nub([2, 4, 1, 3, 3, 1]) = [2, 4, 1, 3]
nub(List) ->
    lists:reverse(uniq(List, [], sets:new())).

%% Keeps last occurrences.
%%     bun([2, 4, 1, 3, 3, 1]) = [2, 4, 3, 1]
bun(List) ->
    uniq(lists:reverse(List), [], sets:new()).

uniq([], Result, _Seen) ->
    Result;
uniq([X | Xs], Result, Seen) ->
    case sets:is_element(X, Seen) of
        true ->
            uniq(Xs, Result, Seen);
        false ->
            uniq(Xs, [X | Result], sets:add_element(X, Seen))
    end.
