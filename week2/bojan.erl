%Make a list of words iterating trough lines of text using space , . ! : ' - as separating characters 
my_words([]) -> 
[]; 
my_words([Line | Text]) -> 
[string:tokens(Line, " ,.!:'-") | my_words(Text)].

%remove short words (less than 3 characters) from list of words 
remove_short([]) -> 
[]; 
remove_short([Word | Text]) -> 
if 
length(Word) < 3 -> 
remove_short(Text); 
length(Word) >= 3 -> 
[Word | remove_short(Text)] 
end. 

%make all words upper case 
make_upper([]) -> 
[]; 
make_upper([Word | Text]) -> 
[string:to_upper(Word) | make_upper(Text)].

%remove common words from text 
remove_common([]) -> 
[]; 
remove_common([Word | Text]) -> 
CommonWords = ["THE", "END", "THAT", "HAVE", "NOT", "WITH", "YOU", "THIS", "HIS", "SAY", "HER", "SHE"], 
case lists:member(Word, CommonWords) of 
true -> remove_common(Text); 
false -> [Word | remove_common(Text)] 
end.

%index text file 
word_index(_Word, [], _, Result) -> 
Result; 
word_index(Word, [Line | Text], K, Result) -> 
case lists:member(Word, Line) of 
true -> word_index(Word, Text, K + 1, erlang:insert_element(K, Result, 1)); 
false -> word_index(Word, Text, K + 1, Result) 
end.
