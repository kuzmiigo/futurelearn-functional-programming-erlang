-module(rpsbasics).
-export([beat/1, lose/1, result/2, tournament/2]).

beat(paper) -> scissors;
beat(rock) -> paper;
beat(scissors) -> rock.

lose(paper) -> rock;
lose(rock) -> scissors;
lose(scissors) -> paper.

result(X, Y) ->
    LoseX = lose(X),
    LoseY = lose(Y),
    if
        LoseX == Y -> 1;
        LoseY == X -> -1;
        true -> 0
    end.

tournament(Left, Right) ->
    lists:sum(lists:zipwith(fun result/2, Left, Right)).
