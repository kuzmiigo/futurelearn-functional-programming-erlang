-module(varspatterns).
-export([howManyEqual/3, maxThree/3, xor1/2, xor2/2, xor3/2]).

xor1(true, false) ->
    true;
xor1(false, true) ->
    true;
xor1(_, _) ->
    false.

xor2(X, X) ->
    false;
xor2(_, _) ->
    true.

xor3(X, Y) ->
    not (X and Y) and (X or Y).

maxThree(X, Y, Z) ->
    max(X, max(Y, Z)).

howManyEqual(X, X, X) ->
    3;
howManyEqual(X, X, _) ->
    2;
howManyEqual(X, _, X) ->
    2;
howManyEqual(_, X, X) ->
    2;
howManyEqual(_, _, _) ->
    0.
