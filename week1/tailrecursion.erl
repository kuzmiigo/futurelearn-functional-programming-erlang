-module(tailrecursion).
-export([fib/1, perfect/1]).

fib(N) when N >= 0 ->
    fib(N, 0, 1).

fib(0, L, _) ->
    L;
fib(N, L, M) ->
    fib(N - 1, M, L + M).


perfect(N) when N > 1 ->
    perfect(N, 1, 0).

perfect(N, N, S) ->
    N == S;
perfect(N, D, S) when N rem D == 0 ->
    perfect(N, D + 1, S + D);
perfect(N, D, S) ->
    perfect(N, D + 1, S).
