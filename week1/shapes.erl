%%% Module to work with shapes. Shapes are represented as tuples, where the
%%% first element is the shape type, other elements depend on the type:
%%% {circle, {X, Y}, R}
%%%     circle with centre coordinates (X, Y), radius R;
%%% {rectangle, {X, Y}, H, W
%%%     rectangel with centre coordinates (X, Y), height H, width W;
%%% {triangle, {X1, Y1}, {X2, Y2}, {X3, Y3}}
%%%     triangle with vertices (X1, Y1), (X2, Y2), (X3, Y3).

-module(shapes).
-export([area/1, dist/2, enclose/1, perimeter/1]).

%% Finds the area of the given shape.
%% Example:
%%     > shapes:area({triangle, {0,0}, {3,0}, {3,4}}).
%%     6.0
area({circle, _, R}) ->
    math:pi() * R * R;
area({rectangle, _, H, W}) ->
    H * W;
area({triangle, {X1, Y1}, {X2, Y2}, {X3, Y3}}) ->
    % Get sides (side lengths) from vertices
    {A, B, C} = triangleSides({X1, Y1}, {X2, Y2}, {X3, Y3}),
    S = (A + B + C) / 2,
    math:sqrt(S * (S - A) * (S - B) * (S - C)).

%% Finds the distance between two points.
%% Example:
%%     > shapes:dist({-1,-1},{2,3}).
%%     5.0
dist({X1, Y1}, {X2, Y2}) ->
    math:sqrt(math:pow(X1 - X2, 2) + math:pow(Y1 - Y2, 2)).

%% Finds the smallest enclosing rectangle of the shape.
%% Example:
%%     > shapes:enclose({triangle, {0,0}, {3,0}, {3,4}}).
%%     {rectangle,{1.5,2.0},4,3}
enclose({circle, {X, Y}, R}) ->
    {rectangle, {X, Y}, 2 * R, 2 * R};
enclose({rectangle, {X, Y}, H, W}) ->
    {rectangle, {X, Y}, H, W};
enclose({triangle, {X1, Y1}, {X2, Y2}, {X3, Y3}}) ->
    MinX = min(X1, min(X2, X3)),
    MaxX = max(X1, max(X2, X3)),
    MinY = min(Y1, min(Y2, Y3)),
    MaxY = max(Y1, max(Y2, Y3)),
    % Coordinates of the centre
    CX = (MaxX + MinX) / 2,
    CY = (MaxY + MinY) / 2,
    H = MaxY - MinY,  % Height
    W = MaxX - MinX,  % Width
    {rectangle, {CX, CY}, H, W}.

%% Finds the perimeter of the given shape.
%% Example:
%%     > shapes:perimeter({triangle, {0,0}, {3,0}, {3,4}}).
%%     12.0
perimeter({circle, _, R}) ->
    2 * math:pi() * R;
perimeter({rectangle, _, H, W}) ->
    2 * (H + W);
perimeter({triangle, {X1, Y1}, {X2, Y2}, {X3, Y3}}) ->
    % Get sides (side lengths) from vertices
    {A, B, C} = triangleSides({X1, Y1}, {X2, Y2}, {X3, Y3}),
    A + B + C.

%% Finds triangle sides (side lengths) from its vertices
triangleSides({X1, Y1}, {X2, Y2}, {X3, Y3}) ->
    A = dist({X1, Y1}, {X2, Y2}),
    B = dist({X2, Y2}, {X3, Y3}),
    C = dist({X1, Y1}, {X3, Y3}),
    {A, B, C}.
