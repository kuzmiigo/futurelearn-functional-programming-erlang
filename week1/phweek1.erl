%% @author Peter Harrad

%% ====================================================================
%% exports
%% ====================================================================
-module(phweek1).
-export([area/1]).
-export([perimeter/1]).
-export([enclose/1]).

-export([bitsDR/1,bitsTR/1]).

-export([test_area_circle/0,test_area_rectangle/0,test_area_triangle/0,
		 test_perimeter_circle/0,test_perimeter_rectangle/0,test_perimeter_triangle/0,
		 test_enclose_circle/0,test_enclose_rectangle/0,test_enclose_triangle/0]).
-export([test_area_circle/1,test_area_rectangle/1,test_area_triangle/1,
		 test_perimeter_circle/1,test_perimeter_rectangle/1,test_perimeter_triangle/1,
		 test_enclose_circle/1,test_enclose_rectangle/1,test_enclose_triangle/1]).

-export([test_bitsDR/0,test_bitsDR/1,test_bitsTR/0,test_bitsTR/1]).

%% ==========================================================================
%% shape functions
%%
%% Choosing to represent the triangle as a series of vertices, since both
%% the circle and rectangle definitions from the course use explicit
%% coordinates (also, this aligns with the APIs that I have seen in the past)
%% ==========================================================================
area({circle,{X,Y},R}) ->
	math:pi()*R*R;
area({rectangle,{X,Y},H,W}) ->
	H*W;
%% Uses Heron's formula to calculate the area based on three sides
area({triangle,{X1,Y1},{X2,Y2},{X3,Y3}}) ->
	{S1,S2,S3} = tngl_side_lengths({triangle,{X1,Y1},{X2,Y2},{X3,Y3}}),
	S = (S1 + S2 + S3)/2,
	math:sqrt(S*(S-S1)*(S-S2)*(S-S3)).

perimeter({circle,{X,Y},R}) ->
	math:pi()*2*R;
perimeter({rectangle,{X,Y},H,W}) ->
	2*H + 2*W;
perimeter({triangle,{X1,Y1},{X2,Y2},{X3,Y3}}) ->
	{S1,S2,S3} = tngl_side_lengths({triangle,{X1,Y1},{X2,Y2},{X3,Y3}}),
	S1 + S2 + S3.

enclose({circle,{X,Y},R}) ->
	{rectangle,{X-r,Y-r},2*R,2*R};
enclose({rectangle,{X,Y},H,W}) ->	% feels a little silly to be doing this, but meh
	{rectangle,{X,Y},H,W};
enclose({triangle,{X1,Y1},{X2,Y2},{X3,Y3}}) ->
	L = min(X1, min(X2,X3)),	% Intermediate values make the final formula
	R = max(X1, max(X2,X3)),	% more readable
	B = min(Y1, min(Y2,Y3)),	%
	T = max(Y1, max(Y2,Y3)),	%
	{rectangle,{(L+R)/2,(B+T)/2},T-B,R-L}.

%% Since there are two functions that use the sides of a triangle, calculating 
%% the sides is broken out nto a separate callable function for maintainability
%% Using math:pow just feels cleaner, even though
%% (X2-X1)*(X2-X1) works just as well as math:pow((X2-X1),2)
tngl_side_lengths({triangle,{X1,Y1},{X2,Y2},{X3,Y3}}) ->
	{math:sqrt(math:pow((X2-X1),2) + math:pow((Y2-Y1),2)),
	 math:sqrt(math:pow((X3-X2),2) + math:pow((Y3-Y2),2)),	
	 math:sqrt(math:pow((X1-X3),2) + math:pow((Y1-Y3),2))}.

%% ====================================================================
%% bits functions
%%
%% Technically the assignment wanted a function called bits, but that would
%% have required two modules so to implement a DR and TR. I prefer the tail
%% recursion, but that's just because I'm of the opinion that it's good to
%% use performance-enhancing code where possible, so as to be comfortable
%% with it in more complex cases where it's really needed. In a simple case
%% like this I'm sceptical that the performance difference is important, but
%% then I'm not able to imagine the production use cases for this execrcise
%% ====================================================================
bitsDR(X) when X>1 ->
	X rem 2 + bitsDR(X bsr 1);
bitsDR(1) ->
	1.

bitsTR(X)->
	bitsTR(X,0).

bitsTR(X,P) when X>1 ->
	bitsTR(X bsr 1,P + X rem 2);
bitsTR (1,P) ->
	P+1.

%% ====================================================================
%% Testing
%%
%% The function/0 forms just use some sample data. So just use 
%%
%% test_area_circle(),test_area_rectangle(),test_area_triangle(),
%% test_perimeter_circle(),test_perimeter_rectangle(),test_perimeter_triangle(),
%% test_enclose_circle(),test_enclose_rectangle(),test_enclose_triangle(),
%% test_bitsDR(), test_bitsTR() to see some sample data
%% ====================================================================
test_area_circle() ->
	test_area_circle({circle,{5,5},3}).
test_area_rectangle() ->
	test_area_rectangle({rectangle,{5,5},4,6}).
test_area_triangle() ->
	test_area_triangle({triangle,{15,15},{23,30},{50,25}}).

test_perimeter_circle()->
	test_perimeter_circle({circle,{5,5},3}).
test_perimeter_rectangle() ->
	test_perimeter_rectangle({rectangle,{5,5},4,6}).
test_perimeter_triangle()->
	test_perimeter_triangle({triangle,{15,15},{23,30},{50,25}}).

test_enclose_circle()->
	test_enclose_circle({circle,{5,5},3}).
test_enclose_rectangle()->
	test_enclose_rectangle({rectangle,{5,5},4,6}).
test_enclose_triangle() ->
	test_enclose_triangle({triangle,{15,15},{23,30},{50,25}}).

test_area_circle({circle,{X,Y},R}) ->
	A = area({circle,{X,Y},R}),
	io:format("The area of a circle with radius ~B is ~.2f~n",[R, A]).
test_area_rectangle({rectangle,{X,Y},H,W}) -> 
	io:format("The area of a rectangle with width ~B and height ~B is ~B~n",
			  [W,H, area({rectangle,{X,Y},H,W})]
			 ).
test_area_triangle({triangle,{X1,Y1},{X2,Y2},{X3,Y3}}) -> 
	io:format("The area of a triangle with vertices at (~B,~B),(~B,~B),(~B,~B) is ~.2f~n",
			  [X1,Y1,X2,Y2,X3,Y3, area({triangle,{X1,Y1},{X2,Y2},{X3,Y3}})]
			  ).

test_perimeter_circle({circle,{X,Y},R}) -> 
	io:format("The perimeter of a circle with radius ~B is ~.2f~n",
			  [R, perimeter({circle,{X,Y},R})]
			 ).
test_perimeter_rectangle({rectangle,{X,Y},H,W}) -> 
	io:format("The perimeter of a rectangle with width ~B and height ~B is ~B~n",
			  [H,W, perimeter({rectangle,{X,Y},H,W})]
			 ).
test_perimeter_triangle({triangle,{X1,Y1},{X2,Y2},{X3,Y3}}) -> 
	io:format("The perimeter of a triangle with with vertices at (~B,~B),(~B,~B),(~B,~B) is ~.2f~n",
			  [X1,Y1,X2,Y2,X3,Y3, perimeter({triangle,{X1,Y1},{X2,Y2},{X3,Y3}})]
			 ).
  
test_enclose_circle({circle,{X,Y},R}) -> 
	{rectangle,{Xr,Yr},Hr,Wr} = enclose({circle,{X,Y},R}),
	io:format("A circle with radius ~B and centre at ~B,~B is enclosed by ",[R,X,Y]), 
	io:format("  a rectangle with width ~B and height ~B and centre at ~B,~B~n",[Wr,Hr,Xr,Yr]).
test_enclose_rectangle({rectangle,{X,Y},H,W}) -> 
	io:format("A rectangle with width ~B and height ~B and centre at ~B,~B is enclosed by ",[H,W, X,Y]), 
	io:format("   a rectangle with width ~B and height ~B and centre at ~B,~B~n",[H,W, X,Y]).
test_enclose_triangle({triangle,{X1,Y1},{X2,Y2},{X3,Y3}}) -> 
	{rectangle,{Xr,Yr},Hr,Wr} = enclose({triangle,{X1,Y1},{X2,Y2},{X3,Y3}}),
	io:format("The area of a triangle with with vertices at (~B,~B),(~B,~B),(~B,~B) is enclosed by ",[X1,Y1,X2,Y2,X3,Y3]),
	io:format("  a rectangle with width ~B and height ~B and centre at ~B,~B~n",[Wr,Hr,Xr,Yr]).

test_bitsDR() ->
	test_bitsDR(7),
	test_bitsDR(45),
	test_bitsDR(65535),
	test_bitsDR(27).

test_bitsTR() ->
	test_bitsTR(7),
	test_bitsTR(45),
	test_bitsTR(65535),
	test_bitsTR(27).

test_bitsDR(X) ->
	io:format("~B is ~.2B in binary, which has ~B bits set~n",[X,X, bitsDR(X)]).

test_bitsTR(X) ->
	io:format("~B is ~.2B in binary, which has ~B bits set~n",[X,X, bitsTR(X)]).
