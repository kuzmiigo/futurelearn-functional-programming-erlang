-module(index).
-export([get_file_contents/1, index/1, show_file_contents/1]).

% Stop words, to be ignored during indexing. Just a few words for now.
-define(STOP_WORDS, sets:from_list(["about", "all", "and", "any", "are", "but",
    "did", "done", "for", "none", "not", "that", "the", "these", "this",
    "those", "will"])).

% Used to read a file into a list of lines.
% Example files available in:
%   gettysburg-address.txt (short)
%   dickens-christmas.txt  (long)
  

% Get the contents of a text file into a list of lines.
% Each line has its trailing newline removed.
get_file_contents(Name) ->
    {ok,File} = file:open(Name, [read]),
    Rev = get_all_lines(File, []),
lists:reverse(Rev).

% Auxiliary function for get_file_contents.
% Not exported.
get_all_lines(File, Partial) ->
    case io:get_line(File, "") of
        eof -> file:close(File),
               Partial;
        Line -> {Strip,_} = lists:split(length(Line)-1, Line),
                get_all_lines(File, [Strip|Partial])
    end.

% Show the contents of a list of strings.
% Can be used to check the results of calling get_file_contents.
show_file_contents([L|Ls]) ->
    io:format("~s~n", [L]),
    show_file_contents(Ls);
show_file_contents([]) ->
    ok.    

%% Indexes the given file.
index(Name) ->
    Lines = get_file_contents(Name),
    {_, RawIndex} = lists:foldl(fun process_line/2, {0, #{}}, Lines),
    Index = maps:map(fun (_K, V) -> make_ranges(V, []) end, RawIndex),
    lists:keysort(1, maps:to_list(Index)).

process_line(Line, {PrevLineNo, Index}) ->
    LineNo = PrevLineNo + 1,
    % The split of line into words is simple. Word normalisation is very
    % primitive: just lower case, no processing of endings.
    Words = re:split(string:to_lower(Line), "\\W", [{return, list}, trim]),
    ValidWords = lists:filter(fun is_valid_word/1, Words),
    lists:foldl(fun add_word/2, {LineNo, Index}, ValidWords).

is_valid_word(Word) ->
    length(Word) > 2 andalso not sets:is_element(Word, ?STOP_WORDS).

add_word(Word, {LineNo, Index}) ->
    UpdateFun = fun (L) -> add_line_no(LineNo, L) end,
    {LineNo, maps:update_with(Word, UpdateFun, [LineNo], Index)}.

%% Adds the given line number to the descreasing list of lines numbers
%% (only if the head is different to avoid duplicates).
add_line_no(X, [X | _Xs] = List) -> List;
add_line_no(X, List) -> [X | List].

%% Makes ranges from the given list of numbers by grouping consecutive numbers
%% together.
make_ranges([], Res) -> Res;
make_ranges([X | Xs], []) -> make_ranges(Xs, [{X, X}]);
make_ranges([X | Xs], [{A, B} | T] = Res) ->
    case A - X of
        0 -> make_ranges(Xs, Res);
        1 -> make_ranges(Xs, [{X, B} | T]);
        _ -> make_ranges(Xs, [{X, X} | Res])
    end.
