-module(take).
-export([take/2, takeTR/2]).

-spec take(integer(), [T]) -> [T].

take(_N, []) ->
    [];
take(0, _Xs) ->
    [];
take(N, [X | Xs]) when N > 0 ->
    [X | take(N - 1, Xs)].


takeTR(N, List) ->
    takeTR(N, List, []).

takeTR(0, _L, R) ->
    lists:reverse(R);
takeTR(N, [H | T], R) ->
    takeTR(N - 1, T, [H | R]);
takeTR(_N, [], R) ->
    lists:reverse(R).
