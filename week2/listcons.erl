-module(listcons).
-export([counts/1, counts2/1, double/1, evens/1, median/1, median2/1,
         modes/1]).

%% Doubles the elements of a list of numbers.
double([]) ->
    [];
double([X|Xs]) ->
    [2 * X | double(Xs)].

%% Extracts the even numbers from a list of integers.
evens([]) ->
    [];
evens([X|Xs]) when X rem 2 == 0 ->
    [X | evens(Xs)];
evens([_|Xs]) ->
    evens(Xs).

%% Finds the median of a list of numbers: this is the middle element when the
%% list is ordered (if the list is of even length it averages the middle two).
median(List) when length(List) > 0 ->
    Sorted = lists:sort(List),
    Len = length(Sorted),
    case Len rem 2 of
        0 ->
            (lists:nth(Len div 2    , Sorted) +
             lists:nth(Len div 2 + 1, Sorted)) / 2;
        _ ->
            lists:nth(Len div 2 + 1, Sorted)
    end.

%% Finds the median of a list of numbers.
%% Shorter version, less efficient.
median2(List) when length(List) > 0 ->
    Sorted = lists:sort(List),
    Len = length(Sorted),
    Idx1 = Len div 2 + Len rem 2,  % Odd length: same element
    Idx2 = Len div 2 + 1,
    (lists:nth(Idx1, Sorted) + lists:nth(Idx2, Sorted)) / 2.

%% Counts the occurences of items in the list.
counts(List) ->
    counts(List, dict:new()).

counts([], D) ->
    dict:to_list(D);
counts([X|Xs], D) ->
    counts(Xs, dict:update_counter(X, 1, D)).

%% Counts the occurences of items in the list. Version with fold.
counts2(List) ->
    lists:foldl(fun (X, D) -> orddict:update_counter(X, 1, D) end,
                orddict:new(), List).

%% Returns the updated list of the most frequent values.
mostFrequent(X, C, {MaxCount, Vals} = M) ->
    if
        C > MaxCount ->
            {C, [X]};
        C < MaxCount ->
            M;
        C == MaxCount ->
            {MaxCount, [X | Vals]}
    end.

%% Finds the modes of a list of numbers: this is a list consisting of the
%% numbers that occur most frequently in the list; if there is is just one,
%% this will be a list with one element only.
modes(List) ->
    Counts = counts2(List),
    {_, Vals} = orddict:fold(fun mostFrequent/3, {0, []}, Counts),
    lists:reverse(Vals).
