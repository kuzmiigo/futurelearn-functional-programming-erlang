-module(palindrome).
-export([palindrome/1]).

%% Checks if the given string is a palindrome.
%%     palindrome("Madam I\'m Adam.") = true
palindrome(List) ->
    Letters = re:replace(List, "[^A-Za-z]", "", [global, {return, list}]),
    LowerCased = string:to_lower(Letters),
    LowerCased == lists:reverse(LowerCased).
