-module(hof).
-export([add/1, times/1, compose/2, id/1, iterate/1]).
-export([compose/1, twice/1]).
-export([compose_test/0, iterate_test/0, twice_test/0]).

add(X) ->
    fun(Y) -> X+Y end.

times(X) ->
    fun(Y) ->
	     X*Y end.

compose(F, G) ->
    fun(X) -> F(G(X)) end.

compose(Fs) ->
    lists:foldr(fun compose/2, fun id/1, Fs).

id(X) ->
    X.

twice(F) -> compose(F, F).

iterate(0) ->
    fun id/1;
iterate(N) ->
    fun (F) -> compose(lists:duplicate(N, F)) end.


compose_test() ->
    11 = (compose([fun (X) -> X + 2 end, fun (X) -> X * X end]))(3),
    25 = (compose([fun (X) -> X * X end, fun (X) -> X + 2 end]))(3),
    ok.

iterate_test() ->
    Iterate3 = iterate(3),
    Plus2 = fun (X) -> X + 2 end,
    16 = (Iterate3(Plus2))(10),
    ok.

twice_test() ->
    18 = (hof:twice(fun (X) -> X * 3 end))(2),
    ok.
